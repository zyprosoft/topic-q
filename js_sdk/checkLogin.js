import { isLogin,getToken,getUserInfo,setUserInfo,setToken,tokenRefreshIfNeed,getUserInfoTimestamp,setUserInfoTimestamp } from '@/api/auth.js';
import { login,updateWxUserInfo,getSettingInfo } from '@/api/api.js'
import {
	checkLoginShowing
} from '@/api/utils.js'

function updateUser() {
	//更新信息
	console.log('开始更新信息：')
	// 获取用户信息
	uni.getUserInfo({
		provider: 'weixin',
		success: function(infoRes) {
			console.log(infoRes)
			if (infoRes.errMsg != 'getUserInfo:ok') return false;
			//存储用户信息
			updateWxUserInfo(infoRes.userInfo).then(saveRes=>{
				console.log('update user info success')
				console.log(saveRes)
				setUserInfo(saveRes.data)
				setUserInfoTimestamp()
			}).catch(error=>{
				
			})
		}
	});
}

export function checkLogin() {
	const token = getToken()
	if(token === null || token === undefined || token.length == 0) {
		uni.navigateTo({
			url: '/pages/login/login'
		})
		return
	}
	//检查是否需要刷新Token
	tokenRefreshIfNeed()
	if(!isLogin()) {
		console.log('用户未登陆!')
		//不存在，显示登录
		uni.navigateTo({
			url: '/pages/login/login'
		})
	}
}
