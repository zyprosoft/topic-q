module.exports = [{
		"text": '吉州区',
		children: [{
			text: '不限',
		}]
	},
	{
		"text": '青原区',
		children: [{
			text: '不限',
		}]
	},
	{
		"text": '经开区',
		children: [{
			text: '不限',
		}]
	},
	{
		"text": '吉安县',
		children: [{
				text: '敦厚镇',
			},
			{
				text: '永阳镇',
			},
			{
				text: '天河镇',
			},
			{
				text: '横江镇',
			},
			{
				text: '固江镇',
			},
			{
				text: '万福镇',
			},
			{
				text: '永和镇',
			},
			{
				text: '桐坪镇',
			},
			{
				text: '凤凰镇',
			},
			{
				text: '油田镇',
			},
			{
				text: '敖城镇',
			},
			{
				text: '北源乡',
			},
			{
				text: '大冲乡',
			},
			{
				text: '梅塘乡',
			},
			{
				text: '登龙乡',
			},
			{
				text: '安塘乡',
			},
			{
				text: '官田乡',
			},
			{
				text: '指阳乡',
			}
		]
	},
	{
		"text": '吉水县',
		children: [{
				text: '文峰镇',
			},
			{
				text: '阜田镇',
			},
			{
				text: '盘谷镇',
			},
			{
				text: '枫江镇',
			},
			{
				text: '黄桥镇',
			},
			{
				text: '金滩镇',
			},
			{
				text: '八都镇',
			},
			{
				text: '双村镇',
			},
			{
				text: '醪桥镇',
			},
			{
				text: '螺田镇',
			},
			{
				text: '白水镇',
			},
			{
				text: '丁江镇',
			},
			{
				text: '乌江镇',
			},
			{
				text: '水南镇',
			},
			{
				text: '尚贤乡',
			},
			{
				text: '水田乡',
			},
			{
				text: '冠山乡',
			}
		]
	},
	{
		"text": '峡江县',
		children: [{
				text: '水边镇',
			},
			{
				text: '马埠镇',
			},
			{
				text: '巴邱镇',
			},
			{
				text: '仁和镇',
			},
			{
				text: '砚溪镇',
			},
			{
				text: '罗田镇',
			},
			{
				text: '桐林乡',
			},
			{
				text: '福民乡',
			},
			{
				text: '戈坪乡',
			},
			{
				text: '金坪华侨农场',
			}
		]
	},
	{
		"text": '新干县',
		children: [
			{
				text: '金川镇',
			},
			{
				text: '三湖镇',
			},
			{
				text: '大洋洲镇',
			},
			{
				text: '七琴镇',
			},
			{
				text: '麦斜镇',
			},
			{
				text: '溧江乡',
			},
			{
				text: '桃溪乡',
			},
			{
				text: '城上乡',
			},
			{
				text: '潭丘乡',
			},
			{
				text: '神政桥乡',
			},
			{
				text: '沂江乡',
			}, ,
			{
				text: '界埠乡',
			},
			{
				text: '荷浦乡',
			}
		]
	},
	{
		"text": '永丰县',
		children: [{
				text: '恩江镇',
			},
			{
				text: '坑田镇',
			},
			{
				text: '沿陂镇',
			},
			{
				text: '古县镇',
			},
			{
				text: '瑶田镇',
			},
			{
				text: '藤田镇',
			},
			{
				text: '石马镇',
			},
			{
				text: '沙溪镇',
			},
			{
				text: '佐龙乡',
			}, ,
			{
				text: '八江乡',
			},
			{
				text: '潭城乡',
			},
			{
				text: '鹿冈乡',
			},
			{
				text: '七都乡',
			},
			{
				text: '陶唐乡',
			},
			{
				text: '中村乡',
			},
			{
				text: '上溪乡',
			},
			{
				text: '潭头乡',
			},
			{
				text: '三坊乡',
			},
			{
				text: '上固乡',
			},
			{
				text: '龙冈畲族乡',
			}
		]
	},
	{
		"text": '泰和县',
		children: [{
				text: '澄江镇',
			},
			{
				text: '碧溪镇',
			},
			{
				text: '桥头镇',
			}, {
				text: '禾市镇',
			}, {
				text: '螺溪镇',
			}, {
				text: '苏溪镇',
			},
			{
				text: '马市镇',
			},
			{
				text: '塘洲镇',
			}, {
				text: '冠朝镇',
			}, {
				text: '沙村镇',
			}, {
				text: '老营盘镇',
			}, {
				text: '灌溪镇',
			},
			{
				text: '苑前镇',
			},
			{
				text: '万合镇',
			}, {
				text: '沿溪镇',
			}, {
				text: '石山乡',
			}, {
				text: '南溪乡',
			}, {
				text: '上模乡',
			},
			{
				text: '水槎乡',
			},
			{
				text: '上圮乡',
			},
			{
				text: '中龙乡',
			}
		]
	},
	{
		"text": '遂川县',
		children: [{
				text: '泉江镇',
			},
			{
				text: '雩田镇',
			},
			{
				text: '碧洲镇',
			},
			{
				text: '草林镇',
			},
			{
				text: '堆子前镇',
			},
			{
				text: '左安镇',
			},
			{
				text: '高坪镇',
			},
			{
				text: '大汾镇',
			},
			{
				text: '衙前镇',
			},
			{
				text: '禾源镇',
			},
			{
				text: '汤湖镇',
			},
			{
				text: '珠田乡',
			},
			{
				text: '巾石乡',
			},
			{
				text: '大坑乡',
			},
			{
				text: '枚江乡',
			},
			{
				text: '双桥乡',
			},
			{
				text: '新江乡',
			},
			{
				text: '五斗江乡',
			},
			{
				text: '西溪乡',
			},
			{
				text: '南江乡',
			},
			{
				text: '黄坑乡',
			},
			{
				text: '营盘圩乡',
			}
		]
	},
	{
		"text": '万安县',
		children: [{
				text: '芙蓉镇',
			},
			{
				text: '枧头镇',
			},
			{
				text: '窑头镇',
			},
			{
				text: '百嘉镇',
			},
			{
				text: '高陂镇',
			},
			{
				text: '潞田镇',
			},
			{
				text: '沙坪镇',
			},
			{
				text: '夏造镇',
			},
			{
				text: '罗塘乡',
			},
			{
				text: '弹前乡',
			},
			{
				text: '武术乡',
			},
			{
				text: '宝山乡',
			},
			{
				text: '涧田乡',
			},
			{
				text: '顺峰乡',
			},
			{
				text: '韶口乡',
			}
		]
	},
	{
		"text": '安福县',
		children: [{
				text: '平都镇',
			},
			{
				text: '浒坑镇',
			},
			{
				text: '洲湖镇',
			},
			{
				text: '横龙镇',
			},
			{
				text: '洋溪镇',
			},
			{
				text: '严田镇',
			},
			{
				text: '枫田镇',
			},
			{
				text: '竹江乡',
			},
			{
				text: '瓜畲乡',
			},
			{
				text: '钱山乡',
			},
			{
				text: '赤谷乡',
			},
			{
				text: '山庄乡',
			},
			{
				text: '洋门乡',
			},
			{
				text: '金田乡',
			},
			{
				text: '彭坊乡',
			},
			{
				text: '泰山乡',
			},
			{
				text: '寮塘乡',
			},
			{
				text: '甘洛乡',
			},
			{
				text: '章庄乡',
			}

		]
	},
	{
		"text": '永新县',
		children: [{
				text: '禾川镇',
			},
			{
				text: '石桥镇',
			},
			{
				text: '澧田镇',
			},
			{
				text: '龙门镇',
			},
			{
				text: '沙市镇',
			},
			{
				text: '文竹镇',
			},
			{
				text: '埠前镇',
			},
			{
				text: '怀忠镇',
			},
			{
				text: '高桥楼镇',
			},
			{
				text: '坳南乡',
			},
			{
				text: '曲白乡',
			},
			{
				text: '才丰乡',
			},
			{
				text: '烟阁乡',
			},
			{
				text: '在中乡',
			},
			{
				text: '三湾乡',
			},
			{
				text: '台岭乡',
			},
			{
				text: '龙田乡',
			},
			{
				text: '高溪乡',
			},
			{
				text: '莲洲乡',
			},
			{
				text: '高市乡',
			},
			{
				text: '象形乡',
			},
			{
				text: '芦溪乡',
			}
		]
	},
	{
		text: '井冈山',
		children: [{
				text: '茨坪街道',
			},
			{
				text: '厦坪镇',
			},
			{
				text: '龙市镇',
			},
			{
				text: '古城镇',
			},
			{
				text: '新城镇',
			}
		]
	}
]
