TopicQ开源微社区v1.0.0

1. 支持不同维度的帖子聚合，如“关注”，“热榜”，“最新”，“版块”，“话题”

2. 支持按照权重排序的用户，圈话题，圈子在信息流中随机推荐

3. 支持发布帖子，动态，话题，圈话题，私信，创建圈子等自主内容

4. 支持积分打赏，支持首页活动运营

5. 支持版块粒度的banner运营活动

6. 小程序端自带管理端功能，可便捷运营

7. 支持分版块内容管理

![TopicQ](https://topicq.icodefuture.com/wp-content/uploads/2021/08/TopicQ-300x300.jpg)

[安装指导](https://topicq.icodefuture.com)