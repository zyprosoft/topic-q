/**
 * 开源协议 GPL
 * 版权所有 吉安码动未来信息科技有限公司
 * 微信联系 zyprosoft
*/
const config = {
	env:'dev',
	createCircleScore:20,
	enableAudioContent:0,
	showReadStatus:0,//帖子列表是不是存在已读状态变化
	scoreName:'山竹',
	themeGradual:1,
	themeColor:'blue',
	appName:'Topic Q',
	indexShareTitle:'Topic Q - 开源微话题社区',
	appIntroduce:'Topic Q - 开源微话题社区',
	company:'码动未来信息科技.技术支持',
	contact:'微信/电话: 18565897463',
	techMobile:'18565897463',
	infoMobile:'18565897463',
	customNoMore:'-- 码动未来信息科技 --',
	locationLevel:2,//1吉安市,2省市
	privateMessageOpen:1,//是否打开私信功能
	selfMallOpen:1,//自营橱窗是否打开
	subscribeOpen:1,//付费订阅是否打开，必须先打开selfMallOpen
	redBagPost:1,//悬赏帖是否打开
	praiseCashPost:1,//打赏是否打开
	defaultBackground:'https://cdn.icodefuture.com/topic-background.png',
	aboutUrl:'https://topicq.icodefuture.com/mini/about.html',
	protocolUrl:'https://topicq.icodefuture.com/mini/protocol.html',
	waterMark:'|watermark/2/text/aWNvZGVmdXR1cmU=/font/5a6L5L2T/fontsize/240/fill/I0ZDRjZGNg==/dissolve/100/gravity/SouthEast/dx/10/dy/10',
	dev:{
		'base_url':'http://106.52.123.112:9111',
		'zgw_app_id': 'devjianghu',
		'zgw_app_secret': 'jianghunow',
		'qiniu_url': 'https://up-z2.qiniup.com',
		'qiniu_domain': 'http://devcdn.icodefuture.com/',
		'introudce_url':'',
		'policy_url':'',
	}
}

export default config
