/**
 * 开源协议 GPL
 * 版权所有 吉安码动未来信息科技有限公司
 * 微信联系 zyprosoft
*/

import {
	callService,
	callSafeService
} from './common.js'
import config from './config.js'
import {
	getToken
} from './auth.js'

//======== 加入圈子能力

export function getIndexTotalInfo() 
{
	let interfaceName = 'common.activity.getIndexTotalInfo'
	let param = {
	}
	return callSafeService(interfaceName, param)
}

export function getMyFavoriteActivePostList(pageIndex,pageSize) 
{
	let interfaceName = 'common.post.getMyFavoriteActivePost'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize)
	}
	return callSafeService(interfaceName, param)
}

export function daySignReward() 
{
	let interfaceName = 'common.score.daySignReward'
	let params = {
	}
	return callSafeService(interfaceName, params)
}

export function getAllCircleBySort(pageIndex,pageSize) 
{
	let interfaceName = 'common.circle.getAllCircleBySort'
	let params = {
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	return callSafeService(interfaceName, params)
}

export function getAllSignStatus() 
{
	let interfaceName = 'common.user.getAllSignStatus'
	let params = {
	}
	return callSafeService(interfaceName, params)
}

export function getTopNewsList() 
{
	let interfaceName = 'common.post.getTopNewsList'
	let params = {
	}
	return callSafeService(interfaceName, params)
}

export function getIndexConfigData() 
{
	let interfaceName = 'common.activity.getIndexConfigData'
	let params = {
	}
	return callSafeService(interfaceName, params)
}

export function markNewFansListRead(idList) 
{
	let interfaceName = 'common.user.markNewFansListRead'
	let params = {
		'idList': idList
	}
	return callSafeService(interfaceName, params)
}

export function getMyNewFansList(pageIndex,pageSize) 
{
	let interfaceName = 'common.user.getNewFansList'
	let params = {
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	return callSafeService(interfaceName, params)
}

export function indexAttentionRecommendList() 
{
	let interfaceName = 'common.post.indexAttentionRecommendList'
	let params = {
	}
	return callSafeService(interfaceName, params)
}

export function getActivePraiseList(pageIndex,pageSize) 
{
	let interfaceName = 'common.post.activePraiseList'
	let params = {
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	return callSafeService(interfaceName, params)
}

export function getIndexRecommendTopicList() 
{
	let interfaceName = 'common.circle.getIndexRecommendTopicList'
	let param = {
	}
	return callSafeService(interfaceName, param)
}

export function getActivePostByAttention(pageIndex,pageSize) 
{
	let interfaceName = 'common.post.getActivePostListByAttention'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize)
	}
	return callSafeService(interfaceName, param)
}

export function getActivePostByType(pageIndex,pageSize,type) 
{
	let interfaceName = 'common.post.getActivePostListByType'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize),
		'type':type
	}
	return callSafeService(interfaceName, param)
}

export function getMyCircleList(pageIndex,pageSize)
{
	let interfaceName = 'common.circle.getMyCircleList'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize)
	}
	return callSafeService(interfaceName, param)
}

export function getOtherUserCircleList(pageIndex,pageSize,userId) 
{
	let interfaceName = 'common.circle.getOtherUserCircleList'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize),
		'userId':userId
	}
	return callSafeService(interfaceName, param)
}

export function getHobbyLabels() 
{
	let interfaceName = 'common.user.getHobbyLabels'
	let param = {
	}
	return callSafeService(interfaceName, param)
}

export function getUserListByScore(pageIndex,pageSize) 
{
	let interfaceName = 'common.user.getUserListByScore'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize)
	}
	return callSafeService(interfaceName, param)
}

export function getCircleIndexRecommendList() 
{
	let interfaceName = 'common.circle.getIndexRecommendList'
	let param = {
	}
	return callSafeService(interfaceName, param)
}

export function getMyActivePostList(pageIndex,pageSize) 
{
	let interfaceName = 'common.post.getMyActivePostList'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize)
	}
	return callSafeService(interfaceName, param)
}

export function getOtherActivePostList(pageIndex,pageSize,userId) 
{
	let interfaceName = 'common.post.getActivePostListByUser'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize),
		'userId': userId,
	}
	return callSafeService(interfaceName, param)
}

export function getActivePostDetail(postId) 
{
	let interfaceName = 'common.post.getActivePostDetail'
	let param = {
		'postId': parseInt(postId),
	}
	return callSafeService(interfaceName, param)
}

export function getCircleMemberList(circleId,pageIndex,pageSize) 
{
	let interfaceName = 'common.circle.getCircleMemberListByCircleId'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize),
		'circleId': circleId,
	}
	return callSafeService(interfaceName, param)
}

export function getCircleTopicDetail(topicId) 
{
	let interfaceName = 'common.topic.getCircleTopicDetail'
	let param = {
		'topicId': parseInt(topicId),
	}
	return callSafeService(interfaceName, param)
}

export function getActivePostByCircleTopicId(topicId,pageIndex,pageSize,type) 
{
	let interfaceName = 'common.post.getActivePostByCircleTopicId'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize),
		'topicId': parseInt(topicId),
		'type': type
	}
	return callSafeService(interfaceName, param)
}

export function deleteActivePost(postId,circleId) 
{
	let interfaceName = 'common.post.deleteActivePost'
	let param = {
		'postId': parseInt(postId),
		'circleId': parseInt(circleId)
	}
	return callSafeService(interfaceName, param)
}

export function rewardOther(postId,score) 
{
	let interfaceName = 'common.score.reward'
	let param = {
		'postId': parseInt(postId),
		'score': parseInt(score)
	}
	return callSafeService(interfaceName, param)
}

export function getActivePostCommentList(postId,lastCommentId,num) 
{
	let interfaceName = 'common.comment.getActivePostCommentList'
	let param = {
		'postId': parseInt(postId),
		'lastCommentId': parseInt(lastCommentId),
		'num': num,
	}
	return callSafeService(interfaceName, param)
}

export function getTopicListByCircleId(circleId, pageIndex, pageSize) {
	let interfaceName = 'common.circle.getTopicListByCircleId'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize),
		'circleId': circleId,
	}
	return callSafeService(interfaceName, param)
}

export function getActivePostListByCircle(circleId, pageIndex, pageSize, type) {
	let interfaceName = 'common.post.getActivePostByCircleId'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize),
		'circleId': circleId,
		'type': type
	}
	return callSafeService(interfaceName, param)
}

export function getCircleInfo(circleId) 
{
	let interfaceName = 'common.circle.getCircleInfoById'
	let param = {
		'circleId': parseInt(circleId),
	}
	return callSafeService(interfaceName, param)
}

export function auditJoinApply(applyId,status) 
{
	let interfaceName = 'common.circle.auditJoinApply'
	let param = {
		'applyId': parseInt(applyId),
		'status':status
	}
	return callSafeService(interfaceName, param)
}

export function cancelCircle(circleId) 
{
	let interfaceName = 'common.circle.cancelCircle'
	let param = {
		'circleId': parseInt(circleId),
	}
	return callSafeService(interfaceName, param)
}

export function applyJoinCircle(circleId,password,note) 
{
	let interfaceName = 'common.circle.joinCircle'
	let param = {
		'circleId': parseInt(circleId),
	}
	if(password !== null) {
		param['password'] = password
	}
	if(note !== null) {
		param['note'] = note
	}
	return callSafeService(interfaceName, param)
}

export function getCircleJoinApplyList(pageIndex,pageSize) 
{
	let interfaceName = 'common.circle.getApplyList'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize),
	}
	return callSafeService(interfaceName, param)
}

export function getCircleByCategory(categoryId,pageIndex,pageSize) 
{
	let interfaceName = 'common.circle.getCircleByCategory'
	let param = {
		'categoryId':parseInt(categoryId),
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize),
	}
	return callSafeService(interfaceName, param)
}

export function createOrUpdateCircle(params) 
{
	let interfaceName = 'common.circle.createOrUpdate'
	return callSafeService(interfaceName, params)
}

export function getCircleCategoryList() 
{
	let interfaceName = 'common.circle.getCircleCategoryList'
	let param = {
	}
	return callSafeService(interfaceName, param)
}


//===========

export function getTopicDetail(topicId) 
{
	let interfaceName = 'common.topic.getTopicDetail'
	let param = {
		'topicId': topicId
	}
	return callSafeService(interfaceName, param)
}

export function getAdminUnreadCountInfo() 
{
	let interfaceName = 'admin.user.getUnreadCountInfo'
	let param = {
	}
	return callSafeService(interfaceName, param)
}

export function miniSmsLogin(mobile, code) 
{
	let interfaceName = 'common.user.miniSmsLogin'
	let param = {
		'mobile': mobile,
		'code': code
	}
	// #ifdef MP-QQ
	param['type'] = 'qq'
	// #endif
	// #ifdef MP-TOUTIAO
	param['type'] = 'byte'
	// #endif
	// #ifdef MP-BAIDU
	param['type'] = 'baidu'
	// #endif
	return callSafeService(interfaceName, param)
}

export function miniSendLoginSms(mobile) {
	let interfaceName = 'common.user.miniSendLoginSms'
	let param = {
		'mobile': mobile
	}
	return callSafeService(interfaceName, param)
}

// #ifdef MP-BAIDU
export function baiduLogin(code,mobile) {
	let interfaceName = 'common.user.baiduLogin'
	let param = {
		'code': code,
		'mobile':mobile
	}
	return callSafeService(interfaceName, param)
}
// #endif

// #ifdef MP-QQ
export function qqLogin(code) {
	let interfaceName = 'common.user.qqLogin'
	let param = {
		'code': code
	}
	return callSafeService(interfaceName, param)
}
// #endif

export function getUserAttentionStatus() {
	let interfaceName = 'common.post.getUserAttentionStatus'
	let param = {}
	return callSafeService(interfaceName, param)
}

export function getIndexActivityList(type,forumId) {
	let interfaceName = 'common.activity.getActivityList'
	let param = {}
	if(type !== null && type !== undefined) {
		param['type'] = type
	}
	if(forumId !== null && forumId !== undefined) {
		param['forumId'] = forumId
	}
	return callSafeService(interfaceName, param)
}

export function getForumDetail(forumId) {
	let interfaceName = 'common.post.getForumDetail'
	let param = {
		'forumId': parseInt(forumId)
	}
	return callSafeService(interfaceName, param)
}

export function getUserGroupList() {
	let interfaceName = 'common.user.getUserGroupList'
	return callSafeService(interfaceName, {})
}

export function getUserScoreDetailList(pageIndex, pageSize) {
	let interfaceName = 'common.user.getUserScoreDetail'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize),
	}
	return callSafeService(interfaceName, param)
}

export function daySign() {
	let interfaceName = 'common.user.daySign'
	let param = {}
	return callSafeService(interfaceName, param)
}

//=======加入自身可见性操作=======

export function updateOnlySelfVisible(postId, status) {
	let interfaceName = 'common.post.updateOnlySelfVisible'
	let param = {
		'postId': postId,
		'status': status
	}
	return callSafeService(interfaceName, param)
}

//======加入搜索======

export function searchAll(keyword) {
	let interfaceName = 'common.search.searchAll'
	let param = {
		'keyword': keyword
	}
	return callSafeService(interfaceName, param)
}

//========= 加入话题 ==============

export function getOtherUserFansList(otherUserId, pageIndex, pageSize) {
	let interfaceName = 'common.user.getOtherUserFansList'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize),
		'otherUserId': otherUserId
	}
	return callSafeService(interfaceName, param)
}

export function getMyFansList(pageIndex, pageSize) {
	let interfaceName = 'common.user.getMyFansList'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize)
	}
	return callSafeService(interfaceName, param)
}

export function getOtherAttentionList(pageIndex,pageSize,otherUserId) 
{
	let interfaceName = 'common.user.getOtherUserAttentionList'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize),
		'otherUserId':otherUserId
	}
	return callSafeService(interfaceName, param)
}

export function getUserAttentionList(pageIndex, pageSize) {
	let interfaceName = 'common.user.getUserAttentionList'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize)
	}
	return callSafeService(interfaceName, param)
}

export function attentionUser(otherUserId, status) {
	let interfaceName = 'common.user.attention'
	let param = {
		'otherUserId': parseInt(otherUserId),
		'status': parseInt(status)
	}
	return callSafeService(interfaceName, param)
}

export function getTopicAttentionStatus(topicId) {
	let interfaceName = 'common.topic.getTopicAttentionStatus'
	let param = {
		'topicId': parseInt(topicId)
	}
	return callSafeService(interfaceName, param)
}

export function getAttentionTopicList(pageIndex, pageSize) {
	let interfaceName = 'common.topic.getAttentionTopicList'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize)
	}
	return callSafeService(interfaceName, param)
}

export function attentionTopic(topicId, status) {
	let interfaceName = 'common.topic.attention'
	let param = {
		'topicId': parseInt(topicId),
		'status': parseInt(status)
	}
	return callSafeService(interfaceName, param)
}

export function getTopicList(keyword, pageIndex, pageSize) {
	let interfaceName = 'common.topic.getTopicList'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize)
	}
	if (keyword !== null && keyword !== undefined && keyword.length > 0) {
		param['keyword'] = keyword
	}
	return callSafeService(interfaceName, param)
}

export function createTopic(params) {
	let interfaceName = 'common.topic.createTopic'
	return callSafeService(interfaceName, params)
}

export function getPostListByTopic(topicId, pageIndex, pageSize, type) {
	let interfaceName = 'common.post.getPostListByTopicId'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize),
		'topicId': topicId,
		'type': type
	}
	return callSafeService(interfaceName, param)
}

//======= 动态设置app信息 ==========
export function getAppSettingInfo() {
	let interfaceName = 'common.app.info'
	return callSafeService(interfaceName, {})
}

//======= 加入代金券能力 =============

export function createPostForVoucherPolicy(params) {
	let interfaceName = 'admin.voucher.createPostForVoucherPolicy'
	return callSafeService(interfaceName, params)
}

export function getMyVoucherUseHistoryList(pageIndex, pageSize) {
	let interfaceName = 'common.voucher.getMyVoucherUseHistory'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize)
	}
	return callSafeService(interfaceName, param)
}

export function getVoucherListByOrderGoods(goodsList) {
	let interfaceName = 'common.voucher.getOrderGoodsMatchedVoucherList'
	let param = {
		'goodsList': goodsList
	}
	return callSafeService(interfaceName, param)
}

export function getMyVoucherList(pageIndex, pageSize, status) {
	let interfaceName = 'common.voucher.getMyVoucherList'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize)
	}
	if (status !== null && status !== undefined) {
		param['status'] = status
	}
	return callSafeService(interfaceName, param)
}

export function createVoucher(policyId) {
	let interfaceName = 'common.voucher.createVoucher'
	let param = {
		'policyId': parseInt(policyId),
	}
	return callSafeService(interfaceName, param)
}

export function getPolicyList(pageIndex, pageSize) {
	let interfaceName = 'admin.voucher.getPolicyList'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize)
	}
	return callSafeService(interfaceName, param)
}

export function createPolicy(params) {
	let interfaceName = 'admin.voucher.createPolicy'
	return callSafeService(interfaceName, params)
}

export function getActivityList(pageIndex, pageSize) {
	let interfaceName = 'admin.voucher.getActivityList'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize)
	}
	return callSafeService(interfaceName, param)
}

export function createActivity(params) {
	let interfaceName = 'admin.voucher.createActivity'
	return callSafeService(interfaceName, params)
}

//======= 加入订阅商品购买能力 =========

export function getUserPublishForumList() {
	let interfaceName = 'common.post.getUserPublishForumList'
	return callSafeService(interfaceName, {})
}

export function getUnlockForumSn(forumId, policyId) {
	let interfaceName = 'common.post.getUnlockForumSn'
	let param = {
		'forumId': parseInt(forumId),
		'policyId': parseInt(policyId)
	}
	return callSafeService(interfaceName, param)
}

export function createPostForPolicy(params) {
	let interfaceName = 'admin.forum.createPostForPolicy'
	return callSafeService(interfaceName, params)
}

export function getForumSubscribeVoucherList(pageIndex, pageSize) {
	let interfaceName = 'admin.forum.getForumSubscribeVoucherList'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize)
	}
	return callSafeService(interfaceName, param)
}

export function getMySubscribeVoucherList(pageIndex, pageSize) {
	let interfaceName = 'common.post.getMySubscribeVoucherList'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize)
	}
	return callSafeService(interfaceName, param)
}

export function getSubscribeGoodsList() {
	let interfaceName = 'admin.goods.getSubscribeGoodsList'
	let param = {}
	return callSafeService(interfaceName, param)
}

export function buySubscribe(forumId) {
	let interfaceName = 'common.post.buySubscribe'
	let param = {
		'forumId': forumId
	}
	return callSafeService(interfaceName, param)
}

export function unlockSubscribe(forumId, policyId, unlockSn) {
	let interfaceName = 'common.post.unlockSubscribe'
	let param = {
		'forumId': forumId,
		'policyId': policyId,
		'unlockSn': unlockSn
	}
	return callSafeService(interfaceName, param)
}

//===================================

//============ 加入自营店铺能力 =========

export function shopChangeStatus(shopId, status) {
	let interfaceName = 'admin.shop.changeStatus'
	let param = {
		'shopId': shopId,
		'status': status
	}
	return callSafeService(interfaceName, param)
}

export function addOrderComment(orderNo, content) {
	let interfaceName = 'common.order.addOrderComment'
	let param = {
		'orderNo': orderNo,
		'content': content
	}
	return callSafeService(interfaceName, param)
}

export function addOrderMessage(orderNo, content) {
	let interfaceName = 'common.order.addOrderMessage'
	let param = {
		'orderNo': orderNo,
		'content': content
	}
	return callSafeService(interfaceName, param)
}

export function pddGetBuyInfo(goodsSign, searchId) {
	let interfaceName = 'common.goods.getBuyJumpInfo'
	let params = {
		'goodsSign': goodsSign,
		'searchId': searchId
	}
	return callSafeService(interfaceName, params)
}

export function getThirdRecommendGoodsList() {
	let interfaceName = 'common.goods.getThirdRecommendGoodsList'
	let params = {}
	return callSafeService(interfaceName, params)
}

export function changeRecommendStatus(status) {
	let interfaceName = 'admin.shop.changeRecommendStatus'
	let params = {
		'status': status
	}
	return callSafeService(interfaceName, params)
}

export function mallCreatePost(params) {
	let interfaceName = 'admin.goods.createPost'
	return callSafeService(interfaceName, params)
}

export function getShopDetail(shopId) {
	let interfaceName = 'common.shop.info'
	let param = {
		'shopId': shopId,
	}
	return callSafeService(interfaceName, param)
}

export function refreshShopOrderSummary(shopId) {
	let interfaceName = 'admin.order.refreshShopOrderSummary'
	let param = {
		'shopId': shopId
	}
	return callSafeService(interfaceName, param)
}

export function refreshUserOrderSummary() {
	let interfaceName = 'common.order.refreshUserOrderSummary'
	let param = {}
	return callSafeService(interfaceName, param)
}

export function asyncCheckOrder(orderNo) {
	let interfaceName = 'common.order.asyncCheckOrder'
	let param = {
		'orderNo': orderNo,
	}
	return callSafeService(interfaceName, param)
}

export function payOrder(orderNo) {
	let interfaceName = 'common.order.payOrder'
	let param = {
		'orderNo': orderNo,
	}
	return callSafeService(interfaceName, param)
}

export function addAddress(param) {
	let interfaceName = 'common.user.addAddress'
	return callSafeService(interfaceName, param)
}

export function deleteGoods(goodsId) {
	let interfaceName = 'admin.goods.delete'
	let param = {
		'goodsId': parseInt(goodsId),
	}
	return callSafeService(interfaceName, param)
}

export function userAppreciate(orderNo) {
	let interfaceName = 'common.order.userAppreciate'
	let param = {
		'orderNo': orderNo
	}
	return callSafeService(interfaceName, param)
}

export function checkReceived(orderNo) {
	let interfaceName = 'common.order.updateReceiveStatus'
	let param = {
		'orderNo': orderNo
	}
	return callSafeService(interfaceName, param)
}

export function getUserOrderListByDeliverStatus(status, pageIndex, pageSize) {
	let interfaceName = 'common.order.getUserOrderListByDeliverStatus'
	let param = {
		'status': parseInt(status),
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize)
	}
	return callSafeService(interfaceName, param)
}

export function getUserNotPayOrderList(pageIndex, pageSize) {
	let interfaceName = 'common.order.getUserNotPayOrderList'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize)
	}
	return callSafeService(interfaceName, param)
}

export function getUserFinishOrderList(pageIndex, pageSize) {
	let interfaceName = 'common.order.getUserFinishOrderList'
	let param = {
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize)
	}
	return callSafeService(interfaceName, param)
}

export function getUserOrderTotalInfo() {
	let interfaceName = 'common.order.getUserOrderTotalInfo'
	let param = {}
	return callSafeService(interfaceName, param)
}

export function getShopOrderList(shopId, pageIndex, pageSize) {
	let interfaceName = 'admin.order.getShopOrderList'
	let param = {
		'shopId': shopId,
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize)
	}
	return callSafeService(interfaceName, param)
}

export function getShopOrderSummary(shopId) {
	let interfaceName = 'admin.order.getShopOrderSummary'
	let param = {
		'shopId': shopId
	}
	return callSafeService(interfaceName, param)
}

export function updateOrderDeliverStatus(orderNo, status) {
	let interfaceName = 'admin.order.updateDeliverStatus'
	let param = {
		'orderNo': orderNo,
		'status': status
	}
	return callSafeService(interfaceName, param)
}

export function getShopOrderListByDeliverStatus(shopId, status, pageIndex, pageSize) {
	let interfaceName = 'admin.order.getShopOrderListByDeliverStatus'
	let param = {
		'shopId': shopId,
		'status': parseInt(status),
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize)
	}
	return callSafeService(interfaceName, param)
}

export function getShopFinishOrderList(shopId, pageIndex, pageSize) {
	let interfaceName = 'admin.order.getShopFinishOrderList'
	let param = {
		'shopId': shopId,
		'pageIndex': parseInt(pageIndex),
		'pageSize': parseInt(pageSize)
	}
	return callSafeService(interfaceName, param)
}

export function createOrder(param) {
	let interfaceName = 'common.order.create'
	return callSafeService(interfaceName, param)
}

export function createShop(param) {
	let interfaceName = 'admin.shop.create'
	return callSafeService(interfaceName, param)
}

export function getShopGoodsList(shopId) {
	let interfaceName = 'common.goods.getGoodsListByShopId'
	let param = {
		'shopId': shopId,
	}
	return callSafeService(interfaceName, param)
}

export function updateShop(param) {
	let interfaceName = 'admin.shop.update'
	return callSafeService(interfaceName, param)
}

export function publishShop(shopId) {
	let interfaceName = 'admin.shop.changeStatus'
	let param = {
		'shopId': shopId,
		'status': 1
	}
	return callSafeService(interfaceName, param)
}

export function pausePublishShop(shopId) {
	let interfaceName = 'admin.shop.changeStatus'
	let param = {
		'shopId': shopId,
		'status': 0
	}
	return callSafeService(interfaceName, param)
}

export function getAllCategoryWithShopId(shopId) {
	let interfaceName = 'admin.category.getAllWithShopId'
	let param = {
		'shopId': shopId
	}
	return callSafeService(interfaceName, param)
}

export function getGoodsListByCategory(categoryId, shopId) {
	let interfaceName = 'common.goods.getGoodsListByCategoryId'
	let param = {
		'shopId': shopId,
		'categoryId': categoryId
	}
	return callSafeService(interfaceName, param)
}

export function createCategory(name, shopId) {
	let interfaceName = 'admin.category.create'
	let param = {
		'name': name,
		'shopId': shopId
	}
	return callSafeService(interfaceName, param)
}

export function updateCategory(name, shopId, categoryId) {
	let interfaceName = 'admin.category.update'
	let param = {
		'name': name,
		'shopId': shopId,
		'categoryId': categoryId
	}
	return callSafeService(interfaceName, param)
}

export function deleteCategory(categoryId, shopId) {
	let interfaceName = 'admin.category.delete'
	let param = {
		'categoryId': categoryId,
		'shopId': shopId
	}
	return callSafeService(interfaceName, param)
}

export function createGoods(param) {
	let interfaceName = 'admin.goods.create'
	return callSafeService(interfaceName, param)
}

export function updateGoods(param) {
	let interfaceName = 'admin.goods.update'
	return callSafeService(interfaceName, param)
}

export function getUnit() {
	let interfaceName = 'admin.goods.allUnit'
	return callSafeService(interfaceName, {})
}

//============ 基础圈子能力 =============

export function pddCreatePost(params) {
	let interfaceName = 'admin.pdd.createPost'
	return callSafeService(interfaceName, params)
}

export function pddRecommend(pageIndex, pageSize, listId) {
	let interfaceName = 'admin.pdd.recommend'
	let params = {
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	if (listId !== null && listId !== undefined) {
		params['listId'] = listId
	}
	return callSafeService(interfaceName, params)
}

export function pddSearch(keyword, pageIndex, pageSize, listId) {
	let interfaceName = 'admin.pdd.search'
	let params = {
		'pageIndex': pageIndex,
		'pageSize': pageSize,
		'keyword': keyword
	}
	if (listId !== null && listId !== undefined) {
		params['listId'] = listId
	}
	return callSafeService(interfaceName, params)
}

export function getMiniProgramCategoryList() {
	let interfaceName = 'admin.third.getMiniProgramCategoryList'
	let params = {}
	return callSafeService(interfaceName, params)
}

export function getOfficialAccountCategoryList() {
	let interfaceName = 'admin.third.getOfficialAccountCategoryList'
	let params = {}
	return callSafeService(interfaceName, params)
}

export function getForum(forumId) {
	let interfaceName = 'admin.forum.getForum'
	let params = {
		'forumId': forumId
	}
	return callSafeService(interfaceName, params)
}

export function getOfficialAccount(accountId) {
	let interfaceName = 'admin.third.getOfficialAccount'
	let params = {
		'accountId': accountId
	}
	return callSafeService(interfaceName, params)
}

export function getMiniProgram(programId) {
	let interfaceName = 'admin.third.getMiniProgram'
	let params = {
		'programId': programId
	}
	return callSafeService(interfaceName, params)
}

export function editForum(params) {
	let interfaceName = 'admin.forum.editForum'
	return callSafeService(interfaceName, params)
}

export function createForum(params) {
	let interfaceName = 'admin.forum.createForum'
	return callSafeService(interfaceName, params)
}

export function editOfficialAccount(params) {
	let interfaceName = 'admin.third.editOfficialAccount'
	return callSafeService(interfaceName, params)
}

export function createOfficialAccount(params) {
	let interfaceName = 'admin.third.createOfficialAccount'
	return callSafeService(interfaceName, params)
}

export function editMiniProgram(params) {
	let interfaceName = 'admin.third.editMiniProgram'
	return callSafeService(interfaceName, params)
}

export function createMiniProgram(params) {
	let interfaceName = 'admin.third.createMiniProgram'
	return callSafeService(interfaceName, params)
}

export function getWaitOperatePostList(pageIndex, pageSize) {
	let interfaceName = 'admin.post.waitOperatePostList'
	let params = {
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	return callSafeService(interfaceName, params)
}

export function getMySubscribeList() {
	let interfaceName = 'common.post.mySubscribeList'
	let params = {}
	return callSafeService(interfaceName, params)
}

export function subscribe(forumId) {
	let interfaceName = 'common.post.subscribe'
	let params = {
		'forumId': forumId
	}
	return callSafeService(interfaceName, params)
}

export function unsubscribe(forumId) {
	let interfaceName = 'common.post.unsubscribe'
	let params = {
		'forumId': forumId
	}
	return callSafeService(interfaceName, params)
}

export function getVideoPostList(type, pageIndex, pageSize) {
	let interfaceName = 'common.post.getVideoPostList'
	let params = {
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	if (type !== null && type !== undefined) {
		params['type'] = type
	}
	return callSafeService(interfaceName, params)
}

export function getPostListBySubscribeForumId(forumId, pageIndex, pageSize,type) {
	let interfaceName = 'common.post.getPostListBySubScribedForumId'
	let params = {
		'pageIndex': pageIndex,
		'pageSize': pageSize,
		'forumId': forumId,
	}
	if(type !== null && type !== undefined) {
		params['type'] = type
	}
	return callSafeService(interfaceName, params)
}

export function getPostListBySubscribe(pageIndex, pageSize) {
	let interfaceName = 'common.post.getPostListBySubScribed'
	let params = {
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	return callSafeService(interfaceName, params)
}

export function getForumList() {
	let interfaceName = 'common.post.getForumList'
	let params = {}
	return callSafeService(interfaceName, params)
}

export function getAlwaysUsedMiniProgramList(pageIndex, pageSize) {
	let interfaceName = 'common.third.getAlwaysUsedMiniProgramList'
	let params = {
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	return callSafeService(interfaceName, params)
}

export function getUsedMiniProgramList(pageIndex, pageSize) {
	let interfaceName = 'common.third.getUsedMiniProgramList'
	let params = {
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	return callSafeService(interfaceName, params)
}

export function unbindMiniProgramOutside(programId) {
	let interfaceName = 'common.third.unbindMiniProgramOutside'
	let params = {
		'programId': programId
	}
	return callSafeService(interfaceName, params)
}

export function markMiniProgramOutside(programId) {
	let interfaceName = 'common.third.markMiniProgramOutside'
	let params = {
		'programId': programId
	}
	return callSafeService(interfaceName, params)
}

export function markOfficialAccountUsed(accountId) {
	let interfaceName = 'common.third.markOfficialAccountUse'
	let params = {
		'accountId': accountId
	}
	return callSafeService(interfaceName, params)
}

export function markMiniProgramUsed(programId) {
	let interfaceName = 'common.third.markMiniProgramUse'
	let params = {
		'programId': programId
	}
	return callSafeService(interfaceName, params)
}

export function getOfficialAccountList() {
	let interfaceName = 'common.third.getOfficialAccountAll'
	return callSafeService(interfaceName, {})
}

export function getMiniProgramList() {
	let interfaceName = 'common.third.getMiniProgramAll'
	return callSafeService(interfaceName, {})
}

export function getStatistic() {
	let interfaceName = 'admin.user.statistic'
	return callSafeService(interfaceName, {})
}

export function markCommentPraiseRead(praiseIds) {
	let interfaceName = 'common.comment.markPraiseRead'
	let params = {
		'praiseIds': praiseIds
	}
	return callSafeService(interfaceName, params)
}

export function markPostPraiseRead(praiseIds) {
	let interfaceName = 'common.post.markPraiseRead'
	let params = {
		'praiseIds': praiseIds
	}
	return callSafeService(interfaceName, params)
}

export function getPostPraiseList(pageIndex, pageSize) {
	let interfaceName = 'common.post.praiseList'
	let params = {
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	return callSafeService(interfaceName, params)
}

export function getCommentPraiseList(pageIndex, pageSize) {
	let interfaceName = 'common.comment.praiseList'
	let params = {
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	return callSafeService(interfaceName, params)
}

export function unbindAvatar() {
	let interfaceName = 'admin.user.unbindAvatar'
	return callSafeService(interfaceName, {})
}

export function chooseAvatar(avatarUserId) {
	let interfaceName = 'admin.user.chooseAvatar'
	let params = {
		'avatarUserId': avatarUserId
	}
	return callSafeService(interfaceName, params)
}

export function getAvatarList(pageIndex, pageSize) {
	let interfaceName = 'admin.user.getAvatarUserList'
	let params = {
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	return callSafeService(interfaceName, params)
}

export function updateAvatarUser(params) {
	let interfaceName = 'admin.user.updateAvatarUser'
	return callSafeService(interfaceName, params)
}

export function createAvatarUser(params) {
	let interfaceName = 'admin.user.createAvatarUser'
	return callSafeService(interfaceName, params)
}

export function refreshConversationUnreadCount(fromUserId) {
	let interfaceName = 'common.message.refreshUnreadMessage'
	let params = {
		'fromUserId': fromUserId
	}
	return callSafeService(interfaceName, params)
}

export function getAdviceList(pageIndex, pageSize, lastId) {
	let interfaceName = 'admin.user.adviceList'
	let params = {
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	if (lastId !== null) {
		params['lastId'] = lastId
	}
	return callSafeService(interfaceName, params)
}

export function sendAdvice(content) {
	let interfaceName = 'common.user.advice'
	let params = {
		'content': content,
	}
	return callSafeService(interfaceName, params)
}

export function lockComment(commentId) {
	let interfaceName = 'admin.comment.managerBlock'
	let params = {
		'commentId': commentId,
	}
	return callSafeService(interfaceName, params)
}

export function lockPost(postId) {
	let interfaceName = 'admin.post.managerBlock'
	let params = {
		'postId': postId
	}
	return callSafeService(interfaceName, params)
}

export function deletePost(postId) {
	let interfaceName = 'common.post.delete'
	let params = {
		'postId': postId
	}
	return callSafeService(interfaceName, params)
}

export function notificationMarkRead(messageIds) {
	let interfaceName = 'common.notification.markRead'
	let params = {
		'messageIds': messageIds
	}
	return callSafeService(interfaceName, params)
}

export function notificationList(pageIndex, pageSize) {
	let interfaceName = 'common.notification.list'
	let params = {
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	return callSafeService(interfaceName, params)
}

export function auditCommentReport(reportId, commentId, status, note) {
	let interfaceName = 'admin.comment.audit'
	let params = {
		'reportId': reportId,
		'commentId': commentId,
		'status': status
	}
	if (note !== null && note !== undefined) {
		params['note'] = note
	}
	return callSafeService(interfaceName, params)
}

export function getReportCommentList(pageIndex, pageSize, lastReportId) {
	let interfaceName = 'admin.comment.getCommentReportList'
	let params = {
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	if (lastReportId !== null && lastReportId !== undefined) {
		params['reportId'] = lastReportId
	}
	return callSafeService(interfaceName, params)
}

export function sortUpPost(postId, status) {
	let interfaceName = 'admin.post.sortUp'
	let params = {
		'postId': postId,
		'status': status
	}
	return callSafeService(interfaceName, params)
}

export function recommendPost(postId, status) {
	let interfaceName = 'admin.post.recommend'
	let params = {
		'postId': postId,
		'status': status
	}
	return callSafeService(interfaceName, params)
}

export function setPostHot(postId, status) {
	let interfaceName = 'admin.post.hot'
	let params = {
		'postId': postId,
		'status': status
	}
	return callSafeService(interfaceName, params)
}

export function getReportPostList(pageIndex, pageSize, lastReportId) {
	let interfaceName = 'admin.post.getPostReportList'
	let params = {
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	if (lastReportId !== null && lastReportId !== undefined) {
		params['reportId'] = lastReportId
	}
	return callSafeService(interfaceName, params)
}

export function auditPostReport(reportId, postId, status, note) {
	let interfaceName = 'admin.post.auditReport'
	let params = {
		'reportId': reportId,
		'postId': Number(postId),
		'status': status
	}
	if (note !== null && note !== undefined) {
		params['note'] = note
	}
	return callSafeService(interfaceName, params)
}

export function auditPost(postId, status, note) {
	let interfaceName = 'admin.post.audit'
	let params = {
		'postId': Number(postId),
		'status': status
	}
	if (note !== null && note !== undefined) {
		params['note'] = note
	}
	return callSafeService(interfaceName, params)
}

export function getWaitAuditPostList(pageIndex, pageSize, lastPostId) {
	let interfaceName = 'admin.post.waitAuditList'
	let params = {
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	if (lastPostId !== null) {
		params['postId'] = lastPostId
	}
	return callSafeService(interfaceName, params)
}

export function userUnreadCountInfo() {
	let interfaceName = 'common.user.unreadCountInfo'
	let params = {}
	return callSafeService(interfaceName, params)
}

export function markReplyRead(commentIds) {
	let interfaceName = 'common.comment.markRead'
	let params = {
		'commentIds': commentIds
	}
	return callSafeService(interfaceName, params)
}

export function markMessageRead(messageIds, fromUserId) {
	let interfaceName = 'common.message.markRead'
	let params = {
		'messageIds': messageIds,
		'fromUserId': fromUserId
	}
	return callSafeService(interfaceName, params)
}

export function getConversationList(pageIndex, pageSize) {
	let interfaceName = 'common.message.conversationList'
	let params = {
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	return callSafeService(interfaceName, params)
}

export function sendMessage(toUserId, content) {
	let interfaceName = 'common.message.create'
	let params = {
		'toUserId': toUserId,
		'content': content
	}
	return callSafeService(interfaceName, params)
}

export function getPrivateMessageList(toUserId, pageIndex, pageSize) {
	let interfaceName = 'common.message.list'
	let params = {
		'toUserId': toUserId,
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	return callSafeService(interfaceName, params)
}


export function getOtherUserCommentList(userId, pageIndex, pageSize) {
	let interfaceName = 'common.comment.getOtherUserCommentList'
	let params = {
		'userId': userId,
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	return callSafeService(interfaceName, params)
}

export function getOtherUserFavoriteList(userId, pageIndex, pageSize) {
	let interfaceName = 'common.post.otherUserFavoriteList'
	let params = {
		'userId': userId,
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	return callSafeService(interfaceName, params)
}

export function getOtherUserPostList(userId, pageIndex, pageSize) {
	let interfaceName = 'common.post.otherUserPostList'
	let params = {
		'userId': userId,
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	return callSafeService(interfaceName, params)
}

export function getOtherUserInfo(userId) {
	let interfaceName = 'common.user.getOtherUserInfo'
	let params = {
		'userId': userId
	}
	return callSafeService(interfaceName, params)
}

export function deleteComment(commentId) {
	let interfaceName = 'common.comment.delete'
	let params = {
		'commentId': Number(commentId),
	}
	return callSafeService(interfaceName, params)
}

export function getUserInfoDetail() {
	let interfaceName = 'common.user.getUserInfo'
	return callSafeService(interfaceName, {})
}

export function updateUserInfo(params) {
	let interfaceName = 'common.user.updateUserInfo'
	return callSafeService(interfaceName, params)
}

export function getUserFavoriteList(pageIndex, pageSize) {
	let interfaceName = 'common.post.favoriteList'
	let params = {
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	return callSafeService(interfaceName, params)
}

export function getUserReplyList(pageIndex, pageSize) {
	let interfaceName = 'common.comment.userReplyList'
	let params = {
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	return callSafeService(interfaceName, params)
}

export function getUserCommentList(pageIndex, pageSize) {
	let interfaceName = 'common.comment.listByUser'
	let params = {
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	return callSafeService(interfaceName, params)
}

export function voteDetail(voteId) {
	let interfaceName = 'common.post.voteDetail'
	let params = {
		'voteId': voteId
	}
	return callSafeService(interfaceName, params)
}

export function vote(postId, voteItemId, voteId) {
	let interfaceName = 'common.post.vote'
	let params = {
		'postId': Number(postId),
		'voteItemId': Number(voteItemId),
		'voteId': voteId
	}
	return callSafeService(interfaceName, params)
}

export function commentDetail(commentId) {
	let interfaceName = 'common.comment.detail'
	let params = {
		'commentId': Number(commentId),
	}
	return callSafeService(interfaceName, params)
}

export function commentReplyList(commentId, pageIndex, pageSize) {
	let interfaceName = 'common.comment.commentReplyList'
	let params = {
		'commentId': commentId,
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	return callSafeService(interfaceName, params)
}

export function postMarkRead(postId) {
	let interfaceName = 'common.post.markRead'
	let params = {
		'postId': postId,
	}
	return callSafeService(interfaceName, params)
}

export function praiseComment(commentId) {
	let interfaceName = 'common.comment.praise'
	let params = {
		'commentId': commentId
	}
	return callSafeService(interfaceName, params)
}

export function reportComment(commentId, content) {
	let interfaceName = 'common.comment.report'
	let params = {
		'commentId': commentId,
		'content': content
	}
	return callSafeService(interfaceName, params)
}

export function replyComment(params) {
	let interfaceName = 'common.comment.reply'
	return callSafeService(interfaceName, params)
}

export function increaseForward(postId) {
	let interfaceName = 'common.post.increaseForward'
	let params = {
		'postId': postId,
	}
	return callSafeService(interfaceName, params)
}

export function praise(postId) {
	let interfaceName = 'common.post.praise'
	let params = {
		'postId': postId,
	}
	return callSafeService(interfaceName, params)
}

export function reportPost(postId, content) {
	let interfaceName = 'common.post.report'
	let params = {
		'postId': postId,
		'content': content
	}
	return callSafeService(interfaceName, params)
}

export function favoritePost(postId) {
	let interfaceName = 'common.post.favorite'
	let params = {
		'postId': postId,
	}
	return callSafeService(interfaceName, params)
}

export function postCommentList(postId, pageIndex, pageSize, sortType) {
	let interfaceName = 'common.comment.list'
	let params = {
		'postId': postId,
		'pageIndex': pageIndex,
		'pageSize': pageSize,
		'type': sortType,
	}
	return callSafeService(interfaceName, params)
}

export function createComment(params) {
	let interfaceName = 'common.comment.create'
	return callSafeService(interfaceName, params)
}

export function getUserPostList(pageIndex, pageSize) {
	let interfaceName = 'common.post.listByUser'
	let params = {
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	return callSafeService(interfaceName, params)
}

export function postList(sortType, pageIndex, pageSize) {
	let interfaceName = 'common.post.list'
	let params = {
		'type': sortType,
		'pageIndex': pageIndex,
		'pageSize': pageSize
	}
	return callSafeService(interfaceName, params)
}

export function postDetail(postId) {
	let interfaceName = 'common.post.detail'
	let params = {
		'postId': postId
	}
	return callSafeService(interfaceName, params)
}

export function updatePost(params) {
	let interfaceName = 'common.post.update'
	return callSafeService(interfaceName, params)
}

export function createPost(params) {
	let interfaceName = 'common.post.create'
	return callSafeService(interfaceName, params)
}

export function refreshToken() {
	if (getToken() === null || getToken() === undefined) {
		return
	}
	let interfaceName = 'common.user.refreshToken'
	let param = {}
	return callSafeService(interfaceName, param)
}

export function decryptPhoneNumber(iv, encryptData) {
	let interfaceName = 'common.user.decryptPhoneNumber'
	let param = {
		'iv': iv,
		'encryptData': encryptData
	}
	return callSafeService(interfaceName, param)
}

export function login(code) {
	let interfaceName = 'common.user.login'
	let param = {
		'code': code
	}
	return callSafeService(interfaceName, param)
}

export function updateWxUserInfo(params) {
	let interfaceName = 'common.user.updateWxUserInfo'
	return callSafeService(interfaceName, params)
}

export async function uploadImage(filePath, fileKey, callback, fail) {

	const current = config.env;
	const currentConfig = config[current];
	let uploadUrl = currentConfig.qiniu_url
	let uploadDomain = currentConfig.qiniu_domain

	console.log('file path:' + filePath)
	let interfaceName = 'common.attactment.getUploadImageToken'
	let param = {
		'fileKey': fileKey
	}
	let result = await callSafeService(interfaceName, param)
	console.log(result)
	let upToken = result.data.token

	console.log('up token:' + upToken)

	uni.uploadFile({
		url: uploadUrl,
		filePath: filePath,
		name: 'file',
		formData: {
			'key': fileKey,
			'token': upToken
		},
		success: (uploadFileRes) => {
			console.log('upload success:')
			console.log(uploadFileRes.data)

			let res = JSON.parse(uploadFileRes.data)
			console.log('key:' + res.key)

			console.log('domain:' + uploadDomain)
			let imgUrl = uploadDomain + res.key
			console.log('imgUrl:' + imgUrl)

			callback(imgUrl)
		},
		fail: (error) => {
			console.log('upload fail:')
			console.log(error)
			fail(error)
		},
		complete: (result) => {
			console.log('upload complete:')
			console.log(result)
		}
	});
}

export async function uploadFile(filePath, fileKey, callback, fail) {

	const current = config.env;
	const currentConfig = config[current];
	let uploadUrl = currentConfig.qiniu_url
	let uploadDomain = currentConfig.qiniu_domain

	console.log('file path:' + filePath)
	let interfaceName = 'common.attactment.getUploadToken'
	let param = {
		'fileKey': fileKey
	}
	let result = await callSafeService(interfaceName, param)
	console.log(result)
	let upToken = result.data.token

	console.log('up token:' + upToken)

	uni.uploadFile({
		url: uploadUrl,
		filePath: filePath,
		name: 'file',
		formData: {
			'key': fileKey,
			'token': upToken
		},
		success: (uploadFileRes) => {
			console.log('upload success:')
			console.log(uploadFileRes.data)

			let res = JSON.parse(uploadFileRes.data)
			console.log('key:' + res.key)

			console.log('domain:' + uploadDomain)
			let imgUrl = uploadDomain + res.key
			console.log('imgUrl:' + imgUrl)

			callback(imgUrl)
		},
		fail: (error) => {
			console.log('upload fail:')
			console.log(error)
			fail(error)
		},
		complete: (result) => {
			console.log('upload complete:')
			console.log(result)
		}
	});
}
