/**
 * 开源协议 GPL
 * 版权所有 吉安码动未来信息科技有限公司
 * 微信联系 zyprosoft
*/
export function formatCount(count) {
	if (count > 1000) {
		let left = count / 1000
		if (left.toString().indexOf('.') !== -1) {
			left = left.toFixed(1)
		}
		return left.toString() + 'k'
	}
	return count
}

export function callUserSubcribeDialog() {
	// #ifdef MP-QQ
	// 长期订阅
	qq.subscribeAppMsg({
		subscribe: true,
		success(res) {
			console.log("----subscribeAppMsg----success", res);
		},
		fail(res) {
			console.log("----subscribeAppMsg----fail", res);
		}
	});
	// #endif
}

export function checkUserSubscribeMessageStatus() {
	// #ifdef MP-QQ
	qq.getSetting({
		withSubscriptions: true,
		success(res) {
			console.log('subscribe setting:', res.authSetting)
			// res.authSetting = {
			//   "scope.userInfo": true,
			//   "scope.userLocation": true
			//   "setting.appMsgSubscribed": true, // 长期订阅开关，true：打开；false：关闭
			// }
			if (res.authSetting['setting.appMsgSubscribed'] == false || res.authSetting[
					'setting.appMsgSubscribed'] == undefined) {
				callUserSubcribeDialog()
			}
		}
	})
	// #endif
}


export function chunk(arr, size) {
	var arr2 = [];
	for (var i = 0; i < arr.length; i = i + size) {
		arr2.push(arr.slice(i, i + size));
	}
	return arr2;
}

export function cutDateFromDateTime(dateTime) {
	var timeArr = dateTime.replace(" ", ":").replace(/\:/g, "-").split("-");
	var nian = timeArr[0];
	var yue = timeArr[1];
	var ri = timeArr[2];
	return nian + '-' + yue + '-' + ri
}

export function getTagColorForRole(roleId) {
	let map = {
		'1': 'orange',
		'2': 'cyan',
		'3': 'mauve',
		'4': 'blue',
		'5': 'grey',
		'6': 'purple',
		'7': 'yellow'
	}
	if (roleId > 7) {
		return 'grey'
	}
	return map[roleId.toString()]
}

export function getTimeDistance(time) {
	// 支持传入10位或13位毫秒数，如 1587367194536,"1587367194"
	// 支持传入日期格式，如 "2020/4/20 15:31:18"

	if (typeof time == "number" || Number(time) == time) {
		if (String(time).length == 10) {
			time = Number(time) * 1000
		} else if (String(time).length == 13) {
			time = Number(time)
		} else {
			console.log("时间格式错误");
			return time;
		}
	} else {
		if (typeof time == "string" && time.split(" ").length == 2 && time.split(/[- : \/]/).length == 6) {
			time = new Date(time.replace(/\-/g, '/')).getTime();
		} else {
			console.log("时间格式错误");
			return time;
		}
	}

	// 处理之后的time为13位数字格式的毫秒数

	var date_now = new Date();
	var date_time = new Date(time);
	var distance = date_now.getTime() - time;

	var days = parseInt(distance / (1000 * 60 * 60 * 24));
	if (days == 1) {
		return "昨天"
	} else if (days > 1 && days < 4) {
		return days + "天前";
	} else if (days > 3) {
		// 超过3天的，返回日期，如 2018-12-05
		// 如果是今年的，就省去年份，返回 12-05
		var year = date_time.getFullYear();
		var month = date_time.getMonth() + 1;
		if (month < 10) {
			month = "0" + month;
		}
		var day = date_time.getDate();
		if (day < 10) {
			day = "0" + day;
		}
		if (date_now.getFullYear() == year) {
			return month + "-" + day;
		} else {
			return year + "-" + month + "-" + day;
		}
	}

	var hours = parseInt((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
	if (hours > 0) {
		return hours + "小时前";
	}

	var minutes = parseInt((distance % (1000 * 60 * 60)) / (1000 * 60));
	if (minutes > 0) {
		return minutes + "分钟前";
	};

	return "刚刚";
}
