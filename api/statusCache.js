/**
 * 开源协议 GPL
 * 版权所有 吉安码动未来信息科技有限公司
 * 微信联系 zyprosoft
*/
export function getCachedAppSettingInfo() 
{
	try{
		let userInfo = uni.getStorageSync('AppSettingInfo')
		console.log('get AppSettingInfo:'+JSON.stringify(userInfo))
		return userInfo
	}catch(e) {
		console.log('fail get AppSettingInfo:'+e)
		return null
	}
}

export function saveAppSettingInfo(info) 
{
	try{
		console.log('saveAppSettingInfo')
		console.log(info)
		let userInfo = uni.setStorageSync('AppSettingInfo',info)
	}catch(e) {
		console.log('fail get AppSettingInfo:'+e)
		return null
	}
}


//保存收货信息
export function saveReceiveInfo(info) 
{
	try{
		console.log('save user receive info')
		console.log(info)
		uni.setStorageSync('UserReceiveInfo',info)
		return true
	}catch(e) {
		console.log('fail set user receive info:'+e)
		return false
	}
}

//获取上一次的收货信息
export function getReceiveInfo() 
{
	try{
		let userInfo = uni.getStorageSync('UserReceiveInfo')
		console.log('get user receive info:'+JSON.stringify(userInfo))
		return userInfo
	}catch(e) {
		console.log('fail get user receive info:'+e)
		return null
	}
}

export function clearPddSearchHistory() 
{
	try{
		uni.removeStorageSync('PddSearchKeywordList')		
	}catch(e) {
		console.log('fail remove PddSearchKeywordList:'+e)
	}
}

export function savePddSearchKeyword(keyword) 
{
	try{
		let list = uni.getStorageSync('PddSearchKeywordList')		
		if(list == null || list == undefined || list == "") {
			list = [keyword]
		}else{
			list.unshift(keyword)
		}
		uni.setStorageSync('PddSearchKeywordList',list)
	}catch(e) {
		console.log('fail get SubscribeTipReadStatus:'+e)
	}
}

export function pddSearchHistoryList() 
{
	try{
		let list = uni.getStorageSync('PddSearchKeywordList')		
		return list
	}catch(e) {
		console.log('fail get PddSearchKeywordList:'+e)
		return null
	}
}

export function saveSubscribeTipReadStatus() 
{
	try{
		console.log('save SubscribeTipReadStatus')
		uni.setStorageSync('SubscribeTipReadStatus','1')
		return true
	}catch(e) {
		console.log('fail set SubscribeTipReadStatus:'+e)
		return false
	}
}

//获取系统设置信息
export function isSubscribeTipReadStatusOk() 
{
	try{
		let status = uni.getStorageSync('SubscribeTipReadStatus')		
		if(status !== null && status !== undefined && status !== "") {
			return true
		}
		return false
	}catch(e) {
		console.log('fail get SubscribeTipReadStatus:'+e)
		return null
	}
}

export function saveMiniProgramOutsideTipReadStatus() 
{
	try{
		console.log('save MiniProgramOutsideTipReadStatus')
		uni.setStorageSync('MiniProgramOutsideTipReadStatus','1')
		return true
	}catch(e) {
		console.log('fail set MiniProgramOutsideTipReadStatus:'+e)
		return false
	}
}

//获取系统设置信息
export function isMiniProgramOutsideTipReadStatusOk() 
{
	try{
		let status = uni.getStorageSync('MiniProgramOutsideTipReadStatus')		
		if(status !== null && status !== undefined && status !== "") {
			return true
		}
		return false
	}catch(e) {
		console.log('fail get MiniProgramOutsideTipReadStatus:'+e)
		return null
	}
}

export function saveOfficialAccountListMode(showType) 
{
	try{
		console.log('save show type')
		console.log(showType)
		uni.setStorageSync('OfficialAccountListMode',showType)
		return true
	}catch(e) {
		console.log('fail set show type:'+e)
		return false
	}
}

//获取系统设置信息
export function getOfficialAccountListMode() 
{
	try{
		let showType = uni.getStorageSync('OfficialAccountListMode')		
		console.log('get OfficialAccountListMode:'+JSON.stringify(showType))
		return showType
	}catch(e) {
		console.log('fail get OfficialAccountListMode:'+e)
		return null
	}
}


export function saveSubscribePostListMode(showType) 
{
	try{
		console.log('save show type')
		console.log(showType)
		uni.setStorageSync('SubscribePostListMode',showType)
		return true
	}catch(e) {
		console.log('fail set show type:'+e)
		return false
	}
}

//获取系统设置信息
export function getSubscribePostListMode() 
{
	try{
		let showType = uni.getStorageSync('SubscribePostListMode')		
		console.log('get SubscribePostListMode:'+JSON.stringify(showType))
		return showType
	}catch(e) {
		console.log('fail get SubscribePostListMode:'+e)
		return null
	}
}

export function saveHomeListMode(showType) 
{
	try{
		console.log('save show type')
		console.log(showType)
		uni.setStorageSync('HomeListMode',showType)
		return true
	}catch(e) {
		console.log('fail set show type:'+e)
		return false
	}
}

//获取系统设置信息
export function getHomeListMode() 
{
	try{
		let showType = uni.getStorageSync('HomeListMode')		
		console.log('get HomeListMode:'+JSON.stringify(showType))
		return showType
	}catch(e) {
		console.log('fail get HomeListMode:'+e)
		return null
	}
}

export function isCircleIndexNeedRefreshNow() 
{
	let lastRefreshTime = getLastCircleIndexRefreshTime() 
	if(lastRefreshTime == null || lastRefreshTime == undefined || lastRefreshTime == "") {
		return false
	}
	let now = parseInt(new Date().getTime()/1000)
	if( now - lastRefreshTime > 60 * 5) {
		console.log('距离上次圈子首页刷新时间过去5分钟，可以主动刷新了!');
		return true
	}
	let passMin = (now - lastRefreshTime) / 60 
	console.log('距离上次圈子首页刷新时间过去'+passMin+'分钟，不需要主动刷新了!');
	return false
}

export function saveLastCircleIndexRefreshTime() 
{
	try{
		console.log('save CircleIndexRefreshTime')
		let timestamp = parseInt(new Date().getTime()/1000)
		console.log(timestamp)
		uni.setStorageSync('CircleIndexRefreshTime',timestamp)
		return true
	}catch(e) {
		console.log('fail set CircleIndexRefreshTime:'+e)
		return false
	}
}

export function getLastCircleIndexRefreshTime() 
{
	try{
		let lastTime = uni.getStorageSync('CircleIndexRefreshTime')		
		console.log('get CircleIndexRefreshTime:'+JSON.stringify(lastTime))
		return lastTime
	}catch(e) {
		console.log('fail get CircleIndexRefreshTime:'+e)
		return null
	}
}

export function isNeedRefreshNow() 
{
	let lastRefreshTime = getLastHomeRefreshTime() 
	if(lastRefreshTime == null || lastRefreshTime == undefined || lastRefreshTime == "") {
		return false
	}
	let now = parseInt(new Date().getTime()/1000)
	if( now - lastRefreshTime > 60 * 10) {
		console.log('距离上次主页刷新时间过去10分钟，可以主动刷新了!');
		return true
	}
	let passMin = (now - lastRefreshTime) / 60 
	console.log('距离上次主页刷新时间过去'+passMin+'分钟，不需要主动刷新了!');
	return false
}

export function saveLastHomeRefreshTime() 
{
	try{
		console.log('save LastHomeRefreshTime')
		let timestamp = parseInt(new Date().getTime()/1000)
		console.log(timestamp)
		uni.setStorageSync('LastHomeRefreshTime',timestamp)
		return true
	}catch(e) {
		console.log('fail set LastHomeRefreshTime:'+e)
		return false
	}
}

export function getLastHomeRefreshTime() 
{
	try{
		let lastTime = uni.getStorageSync('LastHomeRefreshTime')		
		console.log('get LastHomeRefreshTime:'+JSON.stringify(lastTime))
		return lastTime
	}catch(e) {
		console.log('fail get LastHomeRefreshTime:'+e)
		return null
	}
}

export function saveMiniProgramListMode(showType) 
{
	try{
		console.log('save show type')
		console.log(showType)
		uni.setStorageSync('MiniProgramListMode',showType)
		return true
	}catch(e) {
		console.log('fail set show type:'+e)
		return false
	}
}

//获取系统设置信息
export function getMiniProgramListMode() 
{
	try{
		let showType = uni.getStorageSync('MiniProgramListMode')		
		console.log('get MiniProgramListMode:'+JSON.stringify(showType))
		return showType
	}catch(e) {
		console.log('fail get MiniProgramListMode:'+e)
		return null
	}
}

export function saveDraft(draft) 
{
	try{
		console.log('save draft')
		uni.setStorageSync('PostDraft',draft)
		return true
	}catch(e) {
		console.log('fail set PostDraft:'+e)
		return false
	}
}

export function getDraft() 
{
	try{
		let draft = uni.getStorageSync('PostDraft')		
		console.log('get PostDraft:'+JSON.stringify(draft))
		return draft
	}catch(e) {
		console.log('fail get PostDraft:'+e)
		return null
	}
}

export function deleteDraft() 
{
	try{
		let draft = uni.removeStorageSync('PostDraft')	
		console.log('remove PostDraft success!')
	}catch(e) {
		console.log('fail remove PostDraft:'+e)
		return null
	}
}

export function SaveDraftTipReadStatus() 
{
	try{
		console.log('save SaveDraftTipReadStatus')
		uni.setStorageSync('SaveDraftTipReadStatus','1')
		return true
	}catch(e) {
		console.log('fail set SaveDraftTipReadStatus:'+e)
		return false
	}
}

//获取系统设置信息
export function isSaveDraftTipReadStatusOk() 
{
	try{
		let status = uni.getStorageSync('SaveDraftTipReadStatus')		
		if(status !== null && status !== undefined && status !== "") {
			return true
		}
		return false
	}catch(e) {
		console.log('fail get SaveDraftTipReadStatus:'+e)
		return null
	}
}